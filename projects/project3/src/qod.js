// Nicolas Abelanet - 11/07/2022
// qod.js

"use strict";

// Set the quote of the day
async function setQOD() {
    let qod = await getQOD();
    let quote = document.getElementById("quote");
    quote.innerHTML = `"${qod.quote}" -${qod.author}`;
}


// Get the quote of the day from the api.
async function getQOD() {

    // Get the response.
    let response = await fetch("https://quotes.rest/qod")

    // Convert to json.
    let json = await response.json();

    // Retrieve quote.
    let quote = await json.contents.quotes[0];

    return quote
}