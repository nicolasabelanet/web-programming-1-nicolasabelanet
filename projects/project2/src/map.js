// Nicolas Abelanet - 11/07/2022
// map.js

"use strict";

// The canvas
let canvas;

// The current context
let ctx;

// The current level of zoom
let zoomFactor = 1;

// List of random colors
let colorIndex = { value: 0 };
let colors = [
	"#FA2CBC",
	"#D4151A",
	"#88B21D",
	"#88F593",
	"#A78C39",
	"#7CAE05",
	"#288234",
	"#78424A",
	"#00BE17",
	"#E842EB",
	"#696679",
	"#12A740",
	"#A2C932",
	"#12B344",
	"#61DB94",
	"#CA6598",
	"#3772F1",
	"#B81EF8",
	"#37BEE1",
	"#944B0D",
	"#3B9D64",
	"#D1BD25",
	"#45A0D8",
	"#3E552E",
	"#F12ADA",
	"#C88477",
	"#31D072",
	"#DAA242",
	"#1C8FA0",
	"#681CF6",
	"#F0344B",
	"#69E527",
	"#D66611",
	"#315F7A",
	"#FB651E",
	"#AE4672",
	"#C1160E",
	"#9B4E45",
	"#237859",
	"#44A219",
	"#434E58",
	"#74B7D1",
	"#FB6B05",
	"#201323",
	"#9EC19F",
	"#90501C",
	"#69877F",
	"#178703",
	"#B4232C",
	"#CCBCE7",
	"#23A8C6",
	"#2CDC99",
	"#223E22",
	"#C5F93E",
	"#7B3D12",
	"#302C79",
	"#694AAF",
	"#F8E1BB",
	"#99A7DB",
	"#FF0000",
	"#CCCED5",
	"#C5BB46",
	"#CCC734",
	"#83DB0C",
	"#8CECC1",
	"#9C2549",
	"#9202BE",
	"#75EAFD",
	"#9665A2",
	"#8D00BF",
	"#218575",
	"#38B18D",
	"#926864",
	"#1DDEF7",
	"#068393",
	"#AB9782",
	"#AEFA79",
	"#8FF989",
	"#7D28B6",
	"#836F41",
	"#51653E",
	"#BC1B29",
	"#ABB921",
	"#6771BF",
	"#D9DAB6",
	"#2089D0",
	"#1B92E4",
	"#EC0A12",
	"#70D79A",
	"#FE577B",
	"#53650E",
	"#9C2D0F",
	"#DE1107",
	"#B60157",
	"#548969",
	"#75186A",
	"#91074E",
	"#49E038",
	"#886201",
	"#891515",
	"#89460C",
	"#CDBDBA",
	"#A9D139",
	"#0ED676",
	"#FE6DED",
	"#15DF36",
	"#B93684",
	"#AC7FFF",
	"#6FD35D",
	"#4B01EF",
	"#1329FC",
	"#8A1B7F",
	"#95E1E1",
	"#4B23E0",
	"#0FE1B0",
	"#0CFD5B",
	"#A62444",
	"#C46E2C",
	"#C4D5FF",
	"#F4878A",
	"#520402",
	"#8595B8",
	"#0D292F",
	"#8A0B6D",
	"#81DA26",
	"#AE3908",
	"#AFC0F7",
	"#5BF36A",
	"#60ECF8",
	"#ACCD97",
	"#1253B6",
	"#A58147",
	"#0AB172",
	"#17CDED",
	"#241923",
	"#11F551",
	"#7C9A48",
	"#B397EA",
	"#B35D6E",
	"#533811",
	"#B448B7",
	"#D6B151",
	"#FB7C5C",
	"#F18720",
	"#EFEB7A",
	"#94A871",
	"#817896",
	"#0718C2",
	"#77B89F",
	"#136416",
	"#023E5D",
	"#96BAB5",
	"#4586AE",
	"#B41CF1",
	"#9F59DF",
	"#646146",
	"#B12E18",
	"#53E5C9",
	"#F0B75E",
	"#6D721B",
	"#5BE78A",
	"#5100EE",
	"#024A90",
	"#83F1D3",
	"#206A2D",
	"#CC685D",
	"#6C99F2",
	"#C8BE09",
	"#28B5C7",
	"#057F3B",
	"#D36E1C",
	"#D7FF90",
	"#95721F",
	"#352916",
	"#05F0F1",
	"#8372EE",
	"#4E7595",
	"#520A12",
	"#9AFF6C",
	"#75FA4F",
	"#DD5877",
	"#A360FA",
	"#25B705",
	"#61E2F4",
	"#D39E30",
	"#46FB4F",
	"#6CC04F",
	"#2B4793",
	"#79C327",
	"#889F6A",
	"#BE6F79",
	"#A2D69E",
	"#6275DA",
	"#E42381",
	"#9E9BD1",
	"#841578",
	"#3FA638",
	"#55192F",
	"#215601",
	"#B42228",
	"#BB2297",
	"#9A62C3",
	"#F2CF52",
	"#C662FC",
	"#EE3CD6",
	"#EEE152",
	"#93178A",
	"#A01EBD",
	"#C4E6B3",
	"#43BCF3",
	"#AA3B3D",
	"#1C343E",
	"#8D5D96",
	"#0BF1E2",
	"#F135FF",
	"#F0565D",
	"#0EF918",
	"#1DD035",
	"#0963CA",
	"#13F308",
	"#EFD979",
	"#55B2A0",
	"#8EB517",
	"#C28D0C",
	"#80D90E",
	"#397D03",
	"#4AF175",
	"#0D4845",
	"#54CB63",
	"#831072",
	"#CE4A9A",
	"#E2ECFE",
	"#605AE1",
	"#8A024B",
	"#35B361",
	"#36C949",
	"#F09A25",
	"#01893A",
	"#388DB8",
	"#233E9C",
	"#224D08",
	"#1D7095",
	"#4F3262",
	"#2A29AD",
	"#EFDAEE",
	"#1FD3F6",
	"#DD6B22",
	"#E6A9BF",
	"#955B25",
	"#9573F3",
	"#7B9817",
	"#8D3D46",
	"#5F3352",
	"#CC17AB",
	"#2D9FFD",
	"#9E415D",
	"#481964",
	"#BF9B63",
	"#22DEAE",
	"#D0DA82",
	"#BD3777",
	"#5BC703",
	"#C6D1F4",
	"#346CB4",
	"#2C7820",
	"#DC98CA",
	"#06A04F",
	"#19DFB8",
	"#F5077D",
	"#723BE1",
	"#E12FD5",
	"#E649D7",
	"#3BE1E0",
	"#241D5C",
	"#BDACC8",
	"#9DFEC7",
	"#726D7E",
	"#D0AD44",
	"#CDB06E",
	"#5F7F7F",
	"#ADAB99",
	"#D1FDE4",
	"#71B1A7",
	"#B63D5F",
	"#756CD8",
	"#584A15",
	"#C217CC",
	"#75D698",
	"#B8B1C3",
	"#F447D8",
	"#0818C8",
	"#DDCFF3",
	"#952764",
	"#17833C",
	"#8424DA",
	"#0C17C5",
	"#69329C",
	"#E8CFC2",
	"#F74A1D",
]

// List of random business names.
let businessIndex = { value: 0 };
let businesses = [
    "Subsequent Development",
    "The Handsome Miners",
    "Clime Stone Skydiving",
    "Smarty Life",
    "Tactful Investments",
    "City Roll",
    "A Paranormal Dream",
    "Alloy Arc Windows",
    "Cartwheel Kings",
    "Stardust Reef",
    "Horizon State Bank",
    "Master Pipe",
    "Chaparral King",
    "Brew Bean Craft Beer",
    "King Of Blades",
    "Tropical Drift",
    "Barbary Coast Cattle",
    "Scandinavian Brewery",
    "Hush Of Horror",
    "Ore Wealth Corporation",
    "Alison Joffe",
    "Results Are Clear",
    "Vogue Glass",
    "Cultured Bean",
    "Burden Borrowers",
    "Hammer Wood",
    "Dreamy Gardens",
    "Tropical Brew Coffee",
    "The Spice Method",
    "Hydro Blitz",
    "Lovely Forest",
    "Cloud Nine Bar",
    "Scum Dora",
    "Royal Crown Pizzeria",
    "The Fetal Development",
    "Paradigm Trust Company",
    "Money Condo",
    "Magic Touch Credit Repair",
    "Coast Side Solutions",
    "The Haunt Room",
    "Steady Ontogenesis Pro",
    "Three Rivers Auto Glass",
    "Engaging Private",
    "The Fresh Spice",
    "Simp Wide Windows",
    "Meltdown Wonderland",
    "Cash Central",
    "Fine Tuned Financing",
    "Mellow Mushroom Pub",
    "The Cookie Dough Guy",
    "Bridge Funding Group",
    "X-N-Out Burger Bar",
    "Window Solutions",
    "Get Nuzzled",
    "Centennial Glass",
    "Sage Investors",
    "Tours By Alina",
    "Flame Vortex",
    "Shiatsu Fever",
    "The Reading Nook",
    "Terrains Galore",
    "Legacy Measurement",
    "My Random Bear",
    "Galaxy Mining",
    "Integrity Financing",
    "Rex Supply",
    "Fusion Grade",
    "American Iron Mine",
    "Dive Bar Northgate",
    "Happy Move Credit Co",
    "Falcon Mortgage Company",
    "Creative The",
    "Cappa Cale Windows",
    "Total Transportation",
    "Inferno Glass",
    "Stained Glass",
    "Drink in fusion",
    "Safe Lite Auto Glass",
    "Fine Express Financing",
    "Weary Learn Pro",
    "Fitness Shed",
    "Beautiful Nintendo",
    "Sell It All",
    "LA Kings Beef",
    "Glass Outlook",
    "Knuckle Fitness Store",
    "Dream Ocean Ltd",
    "Dreamland Noir",
    "Emergence Collective",
    "Whale Warning",
    "Watchful Wrist",
    "Bauxite Prospecting",
    "Taste Of Take",
    "Lullaby Of Light",
    "A Gentle Chill"
]


function setup() {
    // Set the canvas
    canvas = document.getElementById("canvas");
      
    // Retrieve context
    ctx = canvas.getContext("2d");

    draw();
}

// Note - I am not an artist.
function draw(zoomDelta = 0) {

    if (zoomFactor < .75 && zoomDelta < 0) {
        return;
    }
    
    if (zoomFactor > 2 && zoomDelta > 0) {
        return;
    }
    
    zoomFactor += zoomDelta;
    
    colorIndex.value = 0;
    businessIndex.value = 0;
    
    // Just clearing the rect leaves the lines of the roads... weird.
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // I know this is bad.
    canvas.width = canvas.width;
    
    let roadSize = (canvas.width / 7);

    // Draw the roads
    drawRoad(-140, roadSize);
    drawRoad(375, roadSize);

    // Draw the businesses
    drawBusinesses(-300, 19, canvas.height * 10);
    drawBusinesses(-550, 24, canvas.height * 10);
    drawBusinesses(0, 34, canvas.height * 10);
    drawBusinesses(200, 20, canvas.height * 10);
    drawBusinesses(550, 25, canvas.height * 10);
    drawBusinesses(-750, 40, canvas.height * 10);
        
}


// Draw a road at an x position.
function drawRoad(x, width) {
    // Road
    ctx.fillStyle="rgb(130, 130, 130)"

    let position = new Vector2(x, 0);
    let scale = new Vector2(width, canvas.height / zoomFactor);


    let box = new Box(position, scale, "rgb(130, 130, 130)")
    box.draw()
    

    let start = new Vector2(x, (canvas.height / zoomFactor) / 2);
    let end = new Vector2(x, -start.y);

    let line = new Line(start, end, [10,15], "rgb(255, 255, 0)");
    line.draw();
}   


// Draw a certain number of businesses in
// a column.
function drawBusinesses(x, number, range) {

    number -= 1;
    
    let height = (range / number) / 1.7;
    let width = height; 

    let padding = height;

    range = range - padding * 2;

    let halfHeight = (range / 2);
    
    for (let i = 0; i <= number; i++) {
        let y = (range / number) * (i);

        y -= halfHeight
        y += padding;

        let color = getNext(colors, colorIndex);

        let positon = new Vector2(x, y);
        let scale = new Vector2(width, height);

        let box = new Box(positon, scale, color)

        drawBusiness(box);
    }
    
}


function drawBusiness(box) {

    let labelPosition = box.position;

    // Slightly adjust text to be at the top
    // of the box.
    let textX = labelPosition.x;
    let textY = labelPosition.y - box.scale.y / 1.9;

    let label = new Text(new Vector2(textX, textY), getNext(businesses, businessIndex));

    box.draw();
    label.draw();
}


// Get a random element from an array.
function getNext(items, index) {

    let item = items[index.value];
    index.value += 1;
    index.value %= items.length;

    return item;
}


class Text {
    constructor(position, text) {
        this.position = position;
        this.text = text;
    }

    getPosition() {
        let position = this.position.scaled(zoomFactor);
        return position.transformed();
    }

    draw() {
        let position = this.getPosition();

        let textWidth = ctx.measureText(this.text);

        // Adjust the x coordinate to be at
        // the center.
        let x = position.x - (textWidth.width / 2);
        let y = position.y;
       
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = `${15 * zoomFactor}px serif`;
        ctx.fillText(this.text, x, y);
    }
}


class Line {
    constructor(start, end, dash, color) {
        this.start = start;
        this.end = end;

        this.dash = dash;
        this.color = color;
    }

    // Get the scaled and transformed start.
    getStart() {
        let position = this.start.scaled(zoomFactor);
        return position.transformed();
    }

    // Get the scaled and transformed end.
    getEnd() {
        let position = this.end.scaled(zoomFactor);
        return position.transformed();
    }

    // Get the scaled dash.
    getDash() {
        let dashLength = this.dash[0] * zoomFactor;
        let gapLength = this.dash[1] * zoomFactor;

        return [dashLength, gapLength]; 
    }

    // Draw this line.
    draw() {

        let startPos = this.getStart();
        let endPos = this.getEnd();

        ctx.setLineDash(this.getDash());
        
        ctx.moveTo(startPos.x, startPos.y);
        ctx.lineTo(endPos.x, endPos.y);
        
        ctx.strokeStyle=this.color;
        ctx.stroke();
    }
}



class Box {
    constructor(position, scale, color) {
        this.position = position;
        this.scale = scale;
        this.color = color;
    }
    
    // Get the scaled and transformed position.
    getPosition() {
        let position = this.position.scaled(zoomFactor);
        return position.transformed();
    }
    
    // Get the scaled scale.
    getScale() {
        let scale = this.scale.scaled(zoomFactor);
        return scale;
    }
    
    // Draw this box.
    draw() {
        let position = this.getPosition();
        
        let x = position.x;
        let y = position.y;
        
        let scale = this.getScale();
        
        let width = scale.x;
        let height = scale.y;
        
        // Move center point from top left
        // to center.
        x -= width / 2;
        y -= height / 2;
        
        ctx.fillStyle = this.color;
        ctx.fillRect(x, y, width, height);
    }
}

class Vector2 {
    constructor (x, y) {
        this.x = x;
        this.y = y;
    }

    // Returns a scaled vector.
    scaled(s) {
        let scaledX = this.x * s;
        let scaledY = this.y * s;
        return new Vector2(scaledX, scaledY);
    }

    // Returns a transformed vector.
    transformed() {
        let adjustedX = this.x + canvas.width / 2;
        let adjustedY = this.y + canvas.height / 2;
        return new Vector2(adjustedX, adjustedY);
    }
}