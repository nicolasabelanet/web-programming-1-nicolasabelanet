// Nicolas Abelanet - 11/07/2022
// nav.js

"use strict";

let hidden = false;


function toggleNav() {
    let nav = document.getElementById("nav-content");
    let navButton = document.getElementById("nav-toggle");

    // Hide or show the navigation accordingly.
    if (!hidden) {
        nav.style.display = "None";
        navButton.innerHTML = "Show Navigation"
    }
    else {
        nav.style.display = "Block";
        navButton.innerHTML = "Hide Navigation"
    }

    hidden = !hidden;
}