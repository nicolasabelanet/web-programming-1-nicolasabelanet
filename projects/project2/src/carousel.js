// Nicolas Abelanet - 11/07/2022
// carousel.js

"use strict";

// The list of ids to be present in the carousel.
let ids = [];

// The actual references to the DOM elements.
let sections = [];

// The current index of the visible item in the carousel.
let currentIndex = 0;


function setup(pageIds) {
    ids = pageIds;

    for (let id of ids) {
        sections.push(document.getElementById(id));
    }
    showSection(currentIndex);
}


function next() {
    currentIndex += 1;
    currentIndex %= sections.length;

    showSection(currentIndex);
}


function previous() {
    currentIndex -= 1;

    if (currentIndex < 0) {
        currentIndex = sections.length - 1;
    }

    showSection(currentIndex);
}


function showSection(sectionIndex) {
    
    // Set the display to none for all
    // elements in carousel except the one
    // that is current selected.
    for (let i = 0; i < sections.length; i++) {
        if (i != sectionIndex) {
            sections[i].style.display = "none";
        }
        else {
            sections[i].style.display = "block";
        }
    }
}

