Nicolas Abelanet - 10/13/2022

This is the website for a fictional bookstore called The Reading Nook.
I decided to choose a bookstore because it lends itself nicely to
lots of text content to populate the website.

I wanted to create a set of standards so that whenever styling is added
the website looks uniform and consistent throughout the entire 
site.

The site follows this pattern.

<!-- Nav Bar -->
<!-- Page Content -->
<!-- News -->
<!-- Footer -->

These comments can be seen in the various documents and they are used
to conceptually segment the content. This pattern is present in all pages.

I also created a set of persistent items that exist regardless of what page your 
are on. For example, the navigation bar at the top exists throughout every page
and is exactly the same on every page. I believe elements like this anchor
the site and help people navigate quickly and helps users to better understand the
flow of the site.

In the future I want the news section at the bottom to be
anchored beside whatever content is on the page. I beleive this
would help make the site feel even more uniform and consistent.